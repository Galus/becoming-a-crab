# becoming-a-crab

my first rust project


Who’s this crab, Ferris?

Ferris is the unofficial mascot of the Rust Community. Many Rust programmers call themselves “Rustaceans,” a play on the word “crustacean.” We refer to Ferris with the pronouns “they,” “them,” etc., rather than with gendered pronouns.

Ferris is a name playing off of the adjective, “ferrous,” meaning of or pertaining to iron. Since Rust often forms on iron, it seemed like a fun origin for our mascot’s name!

You can find more images of Ferris on http://rustacean.net/.
